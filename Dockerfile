FROM quay.io/openshift-scale/nginx

COPY index.html /usr/share/nginx/html/128.html

EXPOSE 8080

STOPSIGNAL SIGQUIT

CMD ["nginx", "-g", "daemon off;"]